package br.com.pm.bicicletariog14.model;

import java.time.Instant;
import java.util.UUID;

public class Cobranca {
	private UUID id;
	private String status;
	private Instant horaSolicitacao;
	private Instant horaFinalizacao;
	private float valor;
	private String ciclista;
	
	

	@Override
	public String toString() {
		return "Cobranca [id=" + id + ", status=" + status + ", horaSolicitacao=" + horaSolicitacao
				+ ", horaFinalizacao=" + horaFinalizacao + ", valor=" + valor + ", ciclista=" + ciclista + "]";
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Instant getHoraSolicitacao() {
		return horaSolicitacao;
	}

	public void setHoraSolicitacao(Instant horaSolicitacao) {
		this.horaSolicitacao = horaSolicitacao;
	}

	public Instant getHoraFinalizacao() {
		return horaFinalizacao;
	}

	public void setHoraFinalizacao(Instant horaFinalizacao) {
		this.horaFinalizacao = horaFinalizacao;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public String getCiclista() {
		return ciclista;
	}

	public void setCiclista(String ciclista) {
		this.ciclista = ciclista;
	}

}
