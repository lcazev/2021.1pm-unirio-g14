package br.com.pm.bicicletariog14.controller.dto;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.pm.bicicletariog14.util.JavaMailUtil;

public class EmailDTO {
	@JsonProperty
	String email, mensagem;

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	public EmailDTO() {

	}

	public EmailDTO(String email, String mensagem) {
		super();
		this.email = email;
		this.mensagem = mensagem;
	}

	public void enviarEmail() throws MessagingException {
		if(validaEmail(this.email)) {
			JavaMailUtil.enviarEmail(this.email, this.mensagem);
		} else {
			throw new IllegalArgumentException("Email invalido");
		}
		
	}

	public static boolean validaEmail(String email) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		return matcher.find();
	}

}
