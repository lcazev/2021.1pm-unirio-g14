package br.com.pm.bicicletariog14.controller.dto;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Instant;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.com.pm.bicicletariog14.controller.CobrancaController.CobrancaDTO;
import br.com.pm.bicicletariog14.model.Cobranca;

class CobrancaDTOTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test
	void converterParaCobrancaTest() {
		Instant hora = Instant.now();
		CobrancaDTO cobrancaDTO = new CobrancaDTO();
		cobrancaDTO.id = "6eb8c87e-63c4-49da-9e54-479aaa0b12a5";
		cobrancaDTO.status = "string";
		cobrancaDTO.horaSolicitacao = hora.toString();
		cobrancaDTO.horaFinalizacao = hora.toString();
		cobrancaDTO.valor = 1;
		cobrancaDTO.ciclista = "23";
		
		Cobranca cobranca = cobrancaDTO.converterParaCobranca();
		assertEquals("6eb8c87e-63c4-49da-9e54-479aaa0b12a5", cobranca.getId().toString());
		assertEquals("string", cobranca.getStatus());
		assertEquals(hora, cobranca.getHoraSolicitacao());
		assertEquals(hora, cobranca.getHoraFinalizacao());
		assertEquals(1, cobranca.getValor());
		assertEquals("23", cobranca.getCiclista());
		
	}
	
	@Test
	void criarDTOTest() {
		Instant hora = Instant.now();
		Cobranca cobranca = new Cobranca();
		cobranca.setId(UUID.fromString("6eb8c87e-63c4-49da-9e54-479aaa0b12a5"));
		cobranca.setStatus("string");
		cobranca.setHoraSolicitacao(hora);
		cobranca.setHoraFinalizacao(hora);
		cobranca.setValor(1);
		cobranca.setCiclista("23");
		
		CobrancaDTO cobrancaDTO = new CobrancaDTO(cobranca);
		
		assertEquals("6eb8c87e-63c4-49da-9e54-479aaa0b12a5", cobrancaDTO.id);
		assertEquals("string", cobrancaDTO.status);
		assertEquals(hora.toString(), cobrancaDTO.horaSolicitacao);
		assertEquals(hora.toString(), cobrancaDTO.horaFinalizacao);
		assertEquals(1, cobrancaDTO.valor);
		assertEquals("23", cobrancaDTO.ciclista);
	}

}
