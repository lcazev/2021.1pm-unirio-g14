package br.com.pm.bicicletariog14;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.com.pm.bicicletariog14.util.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;



class ControllerTest {

    private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

    @BeforeAll
    static void init() {
        app.start(7010);
    }

    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void getEchoTest() {
        HttpResponse response = Unirest.get("http://localhost:7010/Artur").asString();
        assertEquals(200, response.getStatus());
        assertEquals("Artur Artur Artur",response.getBody());
    }

    @Test
    void getUserTest(){
        HttpResponse response = Unirest.get("https://run.mocky.io/v3/ff22077d-5b65-4dda-a82b-f73bbfbd7354").asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void getRootTest() {
        HttpResponse response = Unirest.get("http://localhost:7010/").asString();
        assertEquals(200, response.getStatus());
        assertEquals("Isto e um eco, digite algo a mais no caminho",response.getBody());
    }
}
