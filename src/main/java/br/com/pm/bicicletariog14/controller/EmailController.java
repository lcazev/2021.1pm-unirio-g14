package br.com.pm.bicicletariog14.controller;


import br.com.pm.bicicletariog14.controller.dto.EmailDTO;
import io.javalin.http.Context;

public class EmailController {
	
	private EmailController() {}
	public static void enviarEmail(Context ctx) {
		EmailDTO bodyAsClass = ctx.bodyAsClass(EmailDTO.class);
			
		try {
			bodyAsClass.enviarEmail();
		} catch (Exception e) {
			ctx.result(e.getMessage());
		}
	}
}
