package br.com.pm.bicicletariog14.controller.dto;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CartaoDTO {
	@JsonProperty
	String nomeTitular, numero, validade, cvv;

	public CartaoDTO() {

	}

	public CartaoDTO(String nomeTitular, String numero, String validade, String cvv) {
		super();
		this.nomeTitular = nomeTitular;
		this.numero = numero;
		this.validade = validade;
		this.cvv = cvv;
	}

	public void validaNome() {
		if (this.nomeTitular == null || this.nomeTitular.equals("")) {
			throw new IllegalArgumentException("Nome nao pode ser vazio");
		}
		char[] nome = nomeTitular.toCharArray();
		for (char c : nome) {
			if (Character.isDigit(c)) {
				throw new IllegalArgumentException("Nome nao pode conter numeros");
			}
		}
	}

	public void validaNumero() {
		if (this.numero == null || this.numero.equals("")) {
			throw new IllegalArgumentException("Numero nao pode ser vazio");
		}
		if (!numero.matches("[0-9]+")){
			throw new IllegalArgumentException("Numero nao pode conter letras");
		}
	}

	public void validaValidade(){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		if(this.validade == null) {
			throw new IllegalArgumentException("Data nao pode ser vazia");
		}
		try {
			formatter.parse(this.validade);
		} catch(DateTimeParseException e) {
			throw new IllegalArgumentException("Data invalida", e);
		}
		
	}

	public void validaCvv() {
		if(this.cvv == null) {
			throw new IllegalArgumentException("CVV nao pode ser vazio");
		}
		if (!this.cvv.matches("[0-9]+") || this.cvv.length() != 3) {
			throw new IllegalArgumentException("CVV invalido");
		}
	}

}
