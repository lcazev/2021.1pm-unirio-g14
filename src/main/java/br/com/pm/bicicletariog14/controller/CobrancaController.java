package br.com.pm.bicicletariog14.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.pm.bicicletariog14.model.Cobranca;
import io.javalin.http.Context;

public class CobrancaController {

	private static List<Cobranca> listaCobranca = new ArrayList<>();
	
	private CobrancaController(){}

	public static class CobrancaDTO {
		@JsonProperty
		public String id;
		@JsonProperty
		public String status;
		@JsonProperty
		public String horaSolicitacao;
		@JsonProperty
		public String horaFinalizacao;
		@JsonProperty
		public float valor;
		@JsonProperty
		public String ciclista;

		public CobrancaDTO() {

		}

		public CobrancaDTO(Cobranca cobranca) {
			if (cobranca.getId() != null) {
				this.id = cobranca.getId().toString();
			}
			this.status = cobranca.getStatus();
			if (cobranca.getHoraSolicitacao() != null) {
				this.horaSolicitacao = cobranca.getHoraSolicitacao().toString();
			}
			if (cobranca.getHoraFinalizacao() != null) {
				this.horaFinalizacao = cobranca.getHoraFinalizacao().toString();
			}
			this.valor = cobranca.getValor();
			this.ciclista = cobranca.getCiclista();
		}

		public Cobranca converterParaCobranca() {
			Cobranca cobranca = new Cobranca();

			if (this.id != null) {
				cobranca.setId(UUID.fromString(this.id));
				cobranca.setHoraSolicitacao(Instant.parse(this.horaSolicitacao));
				cobranca.setHoraFinalizacao(Instant.parse(this.horaFinalizacao));
			}
			cobranca.setStatus(status);
			cobranca.setValor(valor);
			cobranca.setCiclista(ciclista);
			return cobranca;
		}

	}

	public static void criarCobranca(Context ctx) {
		CobrancaDTO criarCobrancaDTO = ctx.bodyAsClass(CobrancaDTO.class);

		Cobranca cobranca = criarCobrancaDTO.converterParaCobranca();

		listaCobranca.add(cobranca);
		cobranca.setId(UUID.randomUUID());
		cobranca.setHoraSolicitacao(Instant.now());
		cobranca.setHoraFinalizacao(Instant.now());
		ctx.json(new CobrancaDTO(cobranca));
	}

	public static void getCobranca(Context ctx) {
		String idCobrancaString = ctx.pathParam("idCobranca");
		UUID idCobranca;
		try {
			idCobranca = UUID.fromString(idCobrancaString);
		} catch (IllegalArgumentException e) {
			ctx.status(HttpStatus.UNPROCESSABLE_ENTITY_422).html("Dados invalidos");
			return;
		}
		for (Cobranca cobranca : listaCobranca) {
			if (cobranca.getId().equals(idCobranca)) {
				ctx.json(new CobrancaDTO(cobranca));
				return;
			}
		}
		ctx.status(HttpStatus.NOT_FOUND_404).html("Nao encontrado");
	}

}
