package br.com.pm.bicicletariog14.util;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class JavaMailUtil {
	
	private static Session sessao;

	private JavaMailUtil() {
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", true);
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");

		String meuEmail = "br.com.pm.bicicletariog14@gmail.com";
		String senha = "hsgmxsrhjpugaflq";

		Session session = Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(meuEmail, senha);
			}
		});

		setSessao(session);
		session.setDebug(true);
	}

	public static void setSessao(Session sessao) {
		JavaMailUtil.sessao = sessao;
	}

	public static boolean enviarEmail(String destinatario, String mensagem) {
		try {
			Message message = new MimeMessage(sessao);
			message.setFrom(new InternetAddress("br.com.pm.bicicletariog14@gmail.com"));
			Address[] toUser = InternetAddress.parse(destinatario);
			message.setRecipients(Message.RecipientType.TO, toUser);
			message.setSubject("Bicicletario G14 - Externo");
			message.setText(mensagem);
			Transport.send(message);
			return true;
		} catch (MessagingException e) {
			return false;
		}
	}

}
