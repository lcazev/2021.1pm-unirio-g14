package br.com.pm.bicicletariog14.controller;

import br.com.pm.bicicletariog14.controller.dto.CartaoDTO;
import io.javalin.http.Context;

public class CartaoController {
	
	private CartaoController() {}

	public static void validarCartao(Context ctx) {
		CartaoDTO bodyAsClass = ctx.bodyAsClass(CartaoDTO.class);
		try {
			bodyAsClass.validaNome();
			bodyAsClass.validaNumero();
			bodyAsClass.validaValidade();
			bodyAsClass.validaCvv();
		} catch (IllegalArgumentException e) {
			ctx.result(e.getMessage());
		}
	}
}
