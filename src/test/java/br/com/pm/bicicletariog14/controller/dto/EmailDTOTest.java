package br.com.pm.bicicletariog14.controller.dto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EmailDTOTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}
/*
	@Test
	void validaEmail() throws MessagingException {
		EmailDTO email = new EmailDTO("example@gmail.com", "mensagem 123");
		email.enviarEmail();
	}
	*/
	@Test
	void validaEmailInvalido() {
		EmailDTO email = new EmailDTO("example", "mensagem 123");
		assertThrows(IllegalArgumentException.class, () -> {
			email.enviarEmail();
		});
	}

}
