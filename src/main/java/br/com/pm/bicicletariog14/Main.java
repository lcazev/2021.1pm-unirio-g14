package br.com.pm.bicicletariog14;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import br.com.pm.bicicletariog14.controller.CartaoController;
import br.com.pm.bicicletariog14.controller.CobrancaController;
import br.com.pm.bicicletariog14.controller.EmailController;
import io.javalin.Javalin;

public class Main {
	public static void main(String[] args) {
		Javalin app = Javalin.create().routes(() -> {
			path("cobranca", () -> {
				post(CobrancaController::criarCobranca);
				path(":idCobranca", () -> {
					get(CobrancaController::getCobranca);
				});
			});
			path("validaCartaoDeCredito", () -> {
				post(CartaoController::validarCartao);
			});
			path("enviarEmail", () -> {
				post(EmailController::enviarEmail);
			});
			path("filaCobranca", () -> {
				post(CobrancaController::criarCobranca);
			});
		});

		app.start(getHerokuAssignedPort());
	}

	private static int getHerokuAssignedPort() {
		String herokuPort = System.getenv("PORT");
		if (herokuPort != null) {
			return Integer.parseInt(herokuPort);
		}
		return 7000;
	}
}
