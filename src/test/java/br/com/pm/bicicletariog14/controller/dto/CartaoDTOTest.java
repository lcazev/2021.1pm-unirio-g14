package br.com.pm.bicicletariog14.controller.dto;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class CartaoDTOTest {


	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	/*@Test
	void validaNomeValidoTest() {
		CartaoDTO cartao = new CartaoDTO("Joao", null, null, null);
		cartao.validaNome();
	}*/

	@ParameterizedTest
	@ValueSource(strings = { "", "Jo4o" })
	void validaNomeNomesInvalidosTest(String nome) {
		CartaoDTO cartao = new CartaoDTO(nome, null, null, null);
		assertThrows(IllegalArgumentException.class, () -> {
			cartao.validaNome();
		});
	}

	@Test
	void validaNomeNullTest() {
		CartaoDTO cartao = new CartaoDTO(null, null, null, null);
		assertThrows(IllegalArgumentException.class, () -> {
			cartao.validaNome();
		});
	}

	/*@Test
	void validaNumeroValidoTest() {
		CartaoDTO cartao = new CartaoDTO(null, "123456", null, null);
		cartao.validaNumero();
	}*/

	@ParameterizedTest
	@ValueSource(strings = { "", "123a658" })
	void validaNumerosInvalidosTest(String numero) {
		CartaoDTO cartao = new CartaoDTO(numero, null, null, null);
		assertThrows(IllegalArgumentException.class, () -> {
			cartao.validaNome();
		});
	}
	
	@Test
	void validaNumeroNullTest() {
		CartaoDTO cartao = new CartaoDTO(null, null, null, null);
		assertThrows(IllegalArgumentException.class, () -> {
			cartao.validaNumero();
		});
	}


	/*@Test
	void validaValidadeValidaTest() {
		CartaoDTO cartao = new CartaoDTO(null, null, "2021-09-05", null);
		cartao.validaValidade();
	}*/

	@Test
	void validaValidadeNullTest() {
		CartaoDTO cartao = new CartaoDTO(null, null, null, null);
		assertThrows(IllegalArgumentException.class, () -> {
			cartao.validaValidade();
		});
	}

	@Test
	void validaValidadeVaziaTest() {
		CartaoDTO cartao = new CartaoDTO(null, null, "", null);
		assertThrows(IllegalArgumentException.class, () -> {
			cartao.validaValidade();
		});
	}

	/*@Test
	void validaCvvValidoTest() {
		CartaoDTO cartao = new CartaoDTO(null, null, null, "638");
		cartao.validaCvv();
	}*/

	@Test
	void validaCvvNullTest() {
		CartaoDTO cartao = new CartaoDTO(null, null, null, null);
		assertThrows(IllegalArgumentException.class, () -> {
			cartao.validaCvv();
		});
	}
	
	@ParameterizedTest
	@ValueSource(strings = { "", "6a3", "6333" })
	void validaCvvInvalidosTest(String cvv) {
		CartaoDTO cartao = new CartaoDTO(null, null, null, cvv);
		assertThrows(IllegalArgumentException.class, () -> {
			cartao.validaNome();
		});
	}

}
